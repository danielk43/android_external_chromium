#!/usr/bin/env bash

# TODO:
# better webview color

set -e

if [[ -f /.dockerenv ]]
then
  git config --global --add safe.directory "${PWD}"
  for repository in $(dirname $(find . -type d -name .git -printf "%P\n"))
  do
    git config --global --add safe.directory "${PWD}"/"${repository}"
  done
fi

project_root=$(git rev-parse --show-toplevel)
cromite_webview_color="#9EF477"
supported_archs=(arm arm64 x86 x64)
keysize=4096
trichrome=0
linux=0
clean=0
gsync=0
prune=0
patch=0
dname="C = Unknown, ST = Unknown, L = Unknown,\
       O = Unknown, OU = Unknown, CN = Unknown"

usage() {
  echo "Usage:"
  echo "  export CROMITE_KEYSTORE_PASSWORD=<password> # Sign apks"
  echo "  ./build-cromite.sh [ options ]"
  echo
  echo "  Options:"
  echo "    -a <archs> build specified Archs (arm arm64 x86 x64)"
  echo "    -b Cromite project absolute path, cloned if not provided"
  echo "    -c Clean and reset source"
  echo "    -d set flag for Debug build"
  echo "    -g Gclient sync source"
  echo "    -h show this Help message"
  echo "    -k keystore pass Keysize (default: 4096, ignored if exists)"
  echo "    -l build for Linux"
  echo "    -n keystore pass dName (optional: omit for defaults)"
  echo "    -p apply cromite Patches"
  echo "    -r git gc and pRune (caution: time-consuming)"
  echo "    -t package as Trichrome apks (ignored if -l is set)"
  echo "    -u Update checkout to specified ref (branch or tag)"
  echo
  echo "  Example:"
  echo "    ./build-cromite.sh -a \"arm64 x64\" -cgpu -k 3072"
  echo
  exit 1
}

while getopts ":a:b:k:n:u:cdghlprt" opt; do
  case $opt in
    a) build_archs="$OPTARG" ;;
    b) cromite_dir="$OPTARG" ;;
    c) clean=1 ;;
    d) export TARGET_ISDEBUG="true" ;;
    g) gsync=1 ;;
    h) usage ;;
    k) keysize="$OPTARG" ;;
    l) linux=1 ;;
    n) dname="$OPTARG" ;;
    p) patch=1 ;;
    r) prune=1 ;;
    t) trichrome=1 ;;
    u) update="$OPTARG" ;;
    :) echo -e "Option -$OPTARG requires an argument\n"
       usage ;;
    \?) echo -e "Invalid option:-$OPTARG\n"
       usage ;;
  esac
done
shift $((OPTIND-1))

# Generate keystore and get trichrome_certdigest
useKeystore() {
  grep chrome <<< $(git branch --show-current) && export KEYSTORE_PASSWORD=${CROMITE_KEYSTORE_PASSWORD}
  grep calyxos <<< $(git branch --show-current) && export KEYSTORE_PASSWORD=${CALYXOS_KEYSTORE_PASSWORD}
  [[ -z "$KEYSTORE_PASSWORD" ]] && usage
  export USE_KEYSTORE="1"
  export ksname=cromite
  if [[ ! -f "$ksname".keystore ]]; then
    echo -e "$KEYSTORE_PASSWORD\n$KEYSTORE_PASSWORD" | \
    keytool \
    -genkeypair -v \
    -keystore "$ksname".keystore \
    -alias "$ksname" \
    -dname "$dname" \
    -keyalg RSA \
    -keysize "$keysize" \
    -sigalg SHA512withRSA \
    -storetype pkcs12 \
    -validity 10000
  fi
  keystore_path="$project_root"/"$ksname".keystore
  if [[ "$trichrome" == "1" ]]; then
    tccd=$(echo $KEYSTORE_PASSWORD | \
    keytool \
    -export-cert \
    -alias cromite \
    -keystore "$ksname".keystore | \
    sha256sum | \
    awk '{print $1}')
  fi
}

# Reset source
cleanSrc() {
  for dir in "$cromite_dir" "$project_root/src" "$project_root"
  do
    cd $dir
    echo -e "\nClean and Reset $dir"
    for cmd in am cherry-pick merge rebase; do
      git $cmd --abort 2>/dev/null || true
      git submodule foreach git $cmd --abort 2>/dev/null || true
    done
    [[ "$dir" == "$project_root" ]] && clcmd="clean -ffd" || clcmd="clean -ffdx"
    for cmd in "add --all" "reset --hard" "$clcmd"; do
      git $cmd
      git submodule foreach git $cmd 2>/dev/null || true
    done
  done
}

# Address loose object warnings; gc and prune
pruneSrc() {
  sh -c "sync; echo 3 >/proc/sys/vm/drop_caches" 2>/dev/null || true
  for dir in "$cromite_dir" "$project_root/src" "$project_root"
  do
    cd $dir
    echo -e "\nExpire reflog and Prune $dir"
    git reflog expire --all --expire-unreachable=now 2>&1
    git submodule foreach git reflog expire --all --expire-unreachable=now 2>&1 || true
    git gc --aggressive --prune=now 2>&1
    git submodule foreach git gc --aggressive --prune=now 2>&1 || true
  done
}

# Update branch or tag
updateRef() {
  if [[ "$update" == "latest" ]]
  then
    ref=$(curl -sL "https://api.github.com/repos/uazo/cromite/tags" | jq -r .[0].name)
  else
    ref="$update"
  fi
  cd "$cromite_dir"
  git fetch --all --tags --force
  git checkout --force "$ref"
  git pull --force 2> /dev/null || true
  cd "$project_root"
  echo "Checked out ref: $ref"
}

setChromiumVersion() {
  if [[ -z "${CHROMIUM_VERSION}" ]]
  then
    cd "$cromite_dir"
    export CHROMIUM_VERSION=$(< build/RELEASE)
    cd - > /dev/null
  fi
  echo "Chromium Version set to: $CHROMIUM_VERSION"
}

# GClient sync to git tag
gclientSync() {
  # Fetch and check out Chromium version tag
  cd "$project_root/src"
  git fetch --all --tags --force
  git checkout --force "$CHROMIUM_VERSION"

  # Sync before patching
  find . -name index.lock -delete
  cd "$project_root"
  gclient sync -DR --force --with_tags --jobs $(nproc)
  cd src
}

# Apply our changes
patchSrc() {
  echo "Patching $CHROMIUM_VERSION"
  cd "$project_root/src"
  for patchfile in $(cat "$cromite_dir"/build/cromite_patches_list.txt | egrep -v "^#|^$"); do
    patches+="$cromite_dir"/build/patches/"$patchfile"$'\n'
  done
  patches+=$(find "$project_root"/patches -name *.patch | sort)
  for cpatch in $(echo "$patches"); do
    if [[ -f "$cpatch" ]]; then
      if git apply --check "$cpatch" &> /dev/null; then
        git apply "$cpatch";
        echo "Applied: $cpatch";
      elif git apply --reverse --check "$cpatch" &> /dev/null; then
        echo "Already applied: $cpatch";
      elif git apply --check "$cpatch" --3way &> /dev/null; then
        git apply "$cpatch" --3way;
        echo "Applied (as 3way): $cpatch";
      else
        echo -e "\e[0;31mERROR: Cannot apply: $cpatch\e[0m";
      fi
    else
      echo -e "\e[0;31mERROR: Patch doesn't exist: $cpatch\e[0m";
    fi
  done

  # Webview icon rebrand
  echo "Rebranding webview"
  for res in m h xh xxh; do
    icon_path=android_webview/nonembedded/java/res_icon/drawable-"$res"dpi/icon_webview.png
    convert \
    $icon_path \
    -colorspace gray \
    -fill "$cromite_webview_color" \
    -tint 85 \
    $icon_path
  done
}

# Build Chromium from source
buildSrc() {
  if [[ "$linux" == "1" ]]; then
    build/linux/sysroot_scripts/install-sysroot.py --arch="$1"
    gn gen out/"linux-$1" --args="target_os=\"linux\" \
                            $(cat $gn_args) \
                            target_cpu=\"$1\""
    gn args out/"linux-$1" --list > out/"linux-$1"/gn_list
    ninja -C out/"linux-$1" chrome chrome_sandbox
    mkdir chrome-lin-"$1"
    cp out/"linux-$1"/chrome chrome-lin-"$1"
    cp out/"linux-$1"/chrome_sandbox chrome-lin-"$1"
    cp out/"linux-$1"/chrome_100_percent.pak chrome-lin-"$1"
    cp out/"linux-$1"/chrome_200_percent.pak chrome-lin-"$1"
    cp out/"linux-$1"/chrome_crashpad_handler chrome-lin-"$1"
    cp out/"linux-$1"/chrome-wrapper chrome-lin-"$1"
    cp out/"linux-$1"/icudtl.dat chrome-lin-"$1"
    cp out/"linux-$1"/libEGL.so chrome-lin-"$1"
    cp out/"linux-$1"/libGLESv2.so chrome-lin-"$1"
    cp out/"linux-$1"/libqt5_shim.so chrome-lin-"$1"
    cp out/"linux-$1"/libqt6_shim.so chrome-lin-"$1"
    cp out/"linux-$1"/libvk_swiftshader.so chrome-lin-"$1"
    cp out/"linux-$1"/libvulkan.so.1 chrome-lin-"$1"
    cp out/"linux-$1"/product_logo_48.png chrome-lin-"$1"
    cp out/"linux-$1"/resources.pak chrome-lin-"$1"
    cp out/"linux-$1"/snapshot_blob.bin chrome-lin-"$1"
    cp out/"linux-$1"/xdg-mime chrome-lin-"$1"
    cp out/"linux-$1"/xdg-settings chrome-lin-"$1"
    cp -r out/"linux-$1"/locales/ chrome-lin-"$1"
    tar -czf "$project_root"/chrome-lin-"$1".tar.gz chrome-lin-"$1"
    rm -rf chrome-lin-"$1"
  else
    [[ "$1" == "x64" ]] && android_arch="x86_64" || android_arch=$1
    prebuilt_path="$project_root"/prebuilt/"$android_arch"
    mkdir -p "$prebuilt_path" 2>/dev/null || true
    if [[ "$trichrome" == "1" ]]; then
      export REBASELINE_PROGUARD=1
      gn gen out/"$1" --args="target_os=\"android\" \
                        target_cpu=\"$1\" \
                        $(cat $gn_args) \
                        android_keystore_path=\"$keystore_path\" \
                        skip_secondary_abi_for_cq=true \
                        trichrome_certdigest=\"$tccd\" \
                        trichrome_library_package=\"org.cromite.trichromelibrary\""
      gn args out/"$1" --list > out/"$1"/gn_list
      unset b64 && grep -q "64" <<< "$1" && b64="64_"
      ninja -C out/"$1" trichrome_chrome_"$b64"apk \
                        trichrome_library_"$b64"apk \
                        trichrome_webview_"$b64"apk
    else
      chromium_code=$(echo ${CHROMIUM_VERSION#*.*.} | tr -d .)
      if [[ $1 == "arm" ]]; then
        chromium_code+=00
      elif [[ $1 == "arm64" ]]; then
        chromium_code+=50
      elif [[ $1 == "x86" ]]; then
        chromium_code+=10
      elif [[ $1 == "x64" ]]; then
        chromium_code+=60
      fi
      gn gen out/"$1" --args="target_os=\"android\" \
                        target_cpu=\"$1\" \
                        $(cat $gn_args) \
                        skip_secondary_abi_for_cq=true \
                        android_keystore_path=\"$keystore_path\" \
                        android_default_version_code=\"$chromium_code\" \
                        android_default_version_name=\"$CHROMIUM_VERSION\""
      gn args out/"$1" --list > out/"$1"/gn_list
      ninja -C out/"$1" chrome_public_bundle
      ninja -C out/"$1" chrome_public_apk \
                        system_webview_apk
    fi
  fi
  if [[ "$?" == "0" && "$linux" == "0" ]]; then
    for apkpath in $(find out/$1/apks -name "*.apk")
    do
      for pkg in $(basename $apkpath)
      do
        apksigner=$(find third_party/android_sdk -name apksigner)
        "$apksigner" sign \
        --ks "$keystore_path" \
        --ks-pass file:/dev/stdin \
        --ks-key-alias "$ksname" \
        --in "$apkpath" \
        --out "$prebuilt_path"/"$pkg" <<< $KEYSTORE_PASSWORD
      done
    done
    if [[ -n "$b64" ]]; then
      for apk in TrichromeChrome TrichromeLibrary TrichromeWebView
      do
        rm -f "$prebuilt_path"/"$apk".apk
        mv "$prebuilt_path"/"$apk"*.apk "$prebuilt_path"/"$apk".apk
      done
    fi
  fi
}

# Git checkout
if [[ -n "${build_archs}" ]]
then
  if [[ "$linux" == "0" && "$trichrome" == "0" ]]
  then
     git checkout --force chrome
  else
     git checkout --force trichrome
  fi
fi

# Add depot_tools to PATH
if [[ ! $PATH =~ "depot_tools" ]]; then
  echo "Adding depot_tools"
  [[ ! -d depot_tools ]] && git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
  export PATH="$(pwd -P)/depot_tools:$PATH"
fi

if [[ ! -d "$cromite_dir" ]]; then
  echo "Using local Cromite repo"
  [[ ! -d cromite ]] && git clone https://github.com/uazo/cromite.git
  cromite_dir="$project_root"/cromite
fi

gn_args="$cromite_dir"/build/cromite.gn_args

if [[ ! -d src ]]; then
  echo "Initial source download"
  fetch --nohooks android
fi

[[ -n "$update" ]] && updateRef
[[ -n "$build_archs" || "$gsync" == "1" || "$patch" == "1" ]] && setChromiumVersion
[[ -n "$build_archs" && "$linux" == "0" ]] && useKeystore

[[ "$clean" == "1" ]] && cleanSrc
[[ "$prune" == "1" ]] && pruneSrc
[[ "$gsync" == "1" ]] && gclientSync
[[ "$patch" == "1" ]] && patchSrc

cd "$project_root/src"

if [[ -n "$build_archs" ]]; then
  if [[ "$linux" == "1" ]]; then
    build_type="Linux Chrome"
  else
    source build/android/envsetup.sh
    if [[ "$trichrome" == "1" ]]; then
      build_type="Android Trichrome"
    else
      build_type="Android Chrome"
    fi
  fi
  for arch in $(echo "$build_archs" | tr " ," "\n"); do
    if printf "%s\0" "${supported_archs[@]}" | grep -Fqxz "$arch"; then
      echo -e "\nBuilding $build_type $CHROMIUM_VERSION $arch"
      buildSrc "$arch"
    else
      arch_err+="\nUnsupported ARCH: $arch"
    fi
  done
  [[ -n "$arch_err" ]] && echo -e "$arch_err\nSupported ARCHs: ${supported_archs[@]}" && usage
fi
exit 0
