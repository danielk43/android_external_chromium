Cromite Prebuilts
=================
Alternate Cromite browser and webview builder for Linux hosts  
Supports additional architectures and packaging not available in official builds  

Usage
-----
- See documentation for details, prerequisites
    - https://chromium.googlesource.com/chromium/src/+/main/docs/linux/build_instructions.md
    - Chromium source, Cromite, and depot_tools will be cloned if unavailable / unspecified
- Export KEYSTORE_PASSWORD for apk signature
- Execute script with options to build browser / webview
    - Examples:
        - See full usage/help menu  
`./build-cromite.sh -h`
        - Build for Linux (arm* latest chrome bins working)  
`./build-cromite.sh -a "arm64 x64" -cglpu`
        - Build Trichrome apk packages to be included in OS  
`./build-cromite.sh -a "arm64 x86 x64" -cgptu`
        - Build ChromePublic apk packages to be included in OS  
`./build-cromite.sh -a "arm64 x86 x64" -cgpu`
        - Git reset, clean, and prune source only   
`./build-cromite.sh -a "arm64 x86 x64" -cr`

Notes
-----
- Clean and GClient sync only once per release
    - subsequent builds for same version use only -a, -l, -t flags
- Patches stay compatible with latest release
- Git history overwritten frequently

Credits
-------
- Cromite project
    - https://github.com/uazo/cromite
- DivestOS project
    - https://github.com/divested-mobile
- GrapheneOS project
    - https://github.com/GrapheneOS
